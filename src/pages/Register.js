import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import {Row, Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){
const {user} = useContext(UserContext)
const navigate = useNavigate()

const [email, setEmail] = useState('')
const [password1, setPassword1] = useState('')
const [password2, setPassword2] = useState('')
const [firstName, setFirstName] = useState('')
const [lastName, setLastName] = useState('')
const [mobileNo, setMobileNo] = useState('')

// to determined if button is disabled or not
const [isActive, setisActive] = useState(false)
// console.log(email)
// console.log(password1)
// console.log(password2)

function registerUser(event){
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			email: email
		})
	})
	.then(response => response.json())
	.then(result => {
		if(result === true){
			Swal.fire ({
				title: 'Oops!',
				icon: 'error',
				text: 'Email is already in use!'
			})
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/JSON'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email: email,
					password1: password1,
				})
			})
			.then(response => response.json())
			.then(result => {
				if(result !== false){
					setFirstName('')
					setLastName('')
					setMobileNo('')
					setEmail('')
					setPassword1('')

					Swal.fire ({
						title: 'Success!',
						icon: 'success',
						text: 'User successfully registered!'
					})

					navigate('/login')
				} else {
					Swal.fire ({
						title: 'Oh no!',
						icon: 'error',
						text: 'Something went wrong!'
					})
				}
			})
		} 
	})

	// Clear out the input fields after form submission
	

	// alert('User successfully registered!')
}

useEffect(() => {
	if((firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
		// Enables the submit button if the form data has been verified
		setisActive(true)
	} else {
		setisActive(false)
	}
}, [firstName, lastName, mobileNo, email, password1, password2])

	return(
		(user.id !== null) ?
				<Navigate to ="/courses"/>
		:
		<Row className="mt-3 mb-3">
		<Form onSubmit = {event => registerUser(event)}>
		<Form.Label><h3>Register</h3></Form.Label>
			  <Form.Group controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
			        type ="text" 
			        placeholder ="Enter your first name" 
			        value = {firstName}
			        onChange = {event => setFirstName(event.target.value)}
			        required
		        />
		      </Form.Group>
		      <Form.Group controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
			        type ="text" 
			        placeholder ="Enter your last name" 
			        value = {lastName}
			        onChange = {event => setLastName(event.target.value)}
			        required
		        />
		      </Form.Group>
			  <Form.Group controlId="mobileNo">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
			        type ="text" 
			        placeholder ="Enter your phone number" 
			        value = {mobileNo}
			        onChange = {event => setMobileNo(event.target.value)}
			        required
		        />
		     </Form.Group>
		     <Form.Group controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
			        type ="email" 
			        placeholder ="Enter email" 
			        value = {email}
			        onChange = {event => setEmail(event.target.value)}
			        required
		        />
		        <Form.Text className="text-muted">
		            We'll never share your email with anyone else.
		        </Form.Text>
		        </Form.Group>

		        <Form.Group controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
	                type ="password" 
	                placeholder ="Password"
	                value = {password1}
			        onChange = {event => setPassword1(event.target.value)}
	                required
		        />
		        </Form.Group>

		        <Form.Group controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
			        type = "password" 
			        placeholder = "Verify Password"
			        value = {password2}
			        onChange = {event => setPassword2(event.target.value)}
			        required
		        />
		        </Form.Group>
			{ isActive ?
				<Button className ="mt-3" variant="primary" type="submit" id="submitBtn">
		            Submit
				</Button>
				:
				<Button className ="mt-3" variant="primary" type="submit" id="submitBtn" disabled>
		            Submit
				</Button>
			}
		</Form>
		</Row>
	)
}