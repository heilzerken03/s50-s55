import {useState, useEffect, useContext} from 'react'
import {Row, Form, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
// Initializes the use of the properties from the UserProvided in App.js file
const {user, setUser} = useContext(UserContext)

const [email, setEmail] = useState('')
const [password, setPassword] = useState('')
// Initialize useNavigate
const navigate = useNavigate()

const [isActive, setIsActive] = useState(false)
// After login from AppNavBar, it will be directed to Home
// Get authorized user's details
const retrieveUser = (token) => {
	fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		headers: {
			Authorization: `Bearer ${token}`
		}
	})
	.then(response => response.json())
	.then(result => {
		console.log(result)

		// Store the user details retrieved from the token into the global user state
		setUser({
			id: result._id,
			isAdmin: result.isAdmin
		})
	})
}
// For Access Token
function authenticate(event){
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/JSON'
		},
		body: JSON.stringify({
			email: email,
			password: password,
		})
	})
	.then(response => response.json())
	.then(result => {
		if(typeof result.accessToken !== "undefined"){
			localStorage.setItem('token', result.accessToken)

			retrieveUser(result.accessToken)

			Swal.fire({
				title: 'Login Successful!',
				icon: 'success',
				text: "Welcome to Zuitt!"
			})
		} else {
			Swal.fire({
				title: 'Authentication Failed!',
				icon: 'error',
				text: 'Sorry, please try again.'
			})
		}
	})	
}	
// 'email' - state, email - input by user
	// localStorage.setItem('email', email)

	// setUser({
	// 	email: localStorage.getItem('email')
	// })

	// setEmail('')
	// setPassword('')

	// navigate('/courses')

useEffect(() => {
	if(email !=='' && password !==''){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password])

	return(
		(user.id !== null) ?
				<Navigate to ="/courses"/>
		:
			<Row className="mt-3 mb-3">
			<Form onSubmit ={event => authenticate(event)}>
				<Form.Label><h3>Login</h3></Form.Label>
			     <Form.Group controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
				        type ="email" 
				        placeholder ="Enter email" 
				        value = {email}
				        onChange = {event => setEmail(event.target.value)}
				        required
			        />
			        </Form.Group>

			        <Form.Group controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
		                type ="password" 
		                placeholder ="Password"
		                value = {password}
				        onChange = {event => setPassword(event.target.value)}
		                required
			        />
			        </Form.Group>

				{ isActive ?
					<Button className ="mt-3" variant="primary" type="submit" id="submitBtn">
			            Submit
					</Button>
					:
					<Button className ="mt-3" variant="primary" type="submit" id="submitBtn" disabled>
			            Submit
					</Button>
				}
			</Form>
			</Row>
		)
}