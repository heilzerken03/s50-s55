import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

export default function CourseCard({course}){
// Destructuring the props
const {name, description, price, _id} = course

// Initialize a 'count' state with a value of zero
let [count, setCount] = useState(0)
let [slots, setSlots] = useState(15)
const [isOpen, setIsOpen] = useState(true)

// function enroll(){
// if (slots !== 0) {
// 	setCount(count + 1)
// 	setSlots(slots - 1)
// } else {
// 	alert('Number of slots has been occupied!')
// 	}
// }
// // Effects in React is just like side effects/effects in real life, where everything something happens within the component, a function or condition runs.
// // You may also listen or watch a specific state for changes instead of watching/listening to the whole component
// useEffect(() => {
// 	if(slots === 0){
// 		setIsOpen(false)
// 	}
// }, [slots])

return (
    <Card>
    <Card.Title className="courseBody text-center bg-info">
      	<h4>{name}</h4>
    </Card.Title>
      <Card.Body>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>

      </Card.Body>
    </Card>
  )
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign and a specific validation for each of them.
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
